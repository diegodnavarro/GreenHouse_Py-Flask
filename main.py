# -*- coding: utf-8 -*-
"""
Created on Mon May 18 11:35:24 2020

@author: diego
"""
import serial #Import the serial library
from flask import Flask, request, Response, render_template
from flask_cors import CORS, cross_origin
from json import dumps
app = Flask(__name__)

@app.route('/')
def hello_w():
    return app.send_static_file('index.html')

serData = serial.Serial('com3', 9600)
ans = {'t':0, 'h':0, 's':0}
@app.route('/get_ser', methods=['GET'])
def monitor():
    global ans
    if serData.inWaiting() > 0:
        myData = str(serData.readline())
        if myData[2] == "T":
            ans["t"] = float(myData[3:7])
        elif myData[2] == "H":
            ans["h"] = float(myData[3:7])
        elif myData[2] == "S":
            ans["s"] = int(myData[3])
        print(ans)
    return Response(dumps(ans), mimetype='application/json')

@app.route('/set_ser', methods=['POST'])
def write():
    x = request.get_json()
    x = x['sprink'].encode('utf-8')
    serData.write((x))
    global ans
    myData = str(serData.readline())
    if myData[2] == "T":
        ans["t"] = float(myData[3:7])
    elif myData[2] == "H":
        ans["h"] = float(myData[3:7])
    elif myData[2] == "S":
        ans["s"] = int(myData[3])
    print(ans)
    return Response(dumps(ans), mimetype='application/json')
if __name__ == '__main__':
    app.run(port = 5000)
